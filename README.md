# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This is a demo repository with docker conatiners a Data Base MySQL and a Web App made with SpringBoot

* Version 1.0

I want to put on practice next technologies:

* FlyWayDB https://flywaydb.org/getstarted/how
* Kubernetes
* Make SpringBoot container wait for DB to start
* Springboot
	* Springboot security
	* healthCheck
	* Endpoint Documentation




### How do I get set up? ###

The application is build with Docker containers. To run move to the root folder and write the next commando to the console:
  
  
```
$docker-composer up --build
```

The application will start on http://localhost:8080, to test http://localhost:8080/client

* Configuration


* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

RepoOwner: Kenneth Alvarado